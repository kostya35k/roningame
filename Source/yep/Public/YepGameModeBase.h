// moe

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "YepGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class YEP_API AYepGameModeBase : public AGameModeBase
{
    GENERATED_BODY()

public:
    AYepGameModeBase();

};
