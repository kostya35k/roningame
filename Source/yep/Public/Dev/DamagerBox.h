// Artem

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "TimerManager.h"
#include "DamagerBox.generated.h"

UCLASS()
class YEP_API ADamagerBox : public ATriggerBox
{
	GENERATED_BODY()
public:
	
	ADamagerBox();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void BeginOnOverlap(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
	void EndOnOverlap(class AActor* OverlappedActor, class AActor* OtherActor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float DamageCooldown = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float DamageAttack = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float ActiveTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
	float ActiveCooldown = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor Color = FColor::Red;

protected:
	virtual void BeginPlay() override;

private:

	float CurrentTime;
	
	bool bIsActive;
	
	FTimerHandle Timer;

	AActor* TargetActor;

	void ResetTimer();
};
