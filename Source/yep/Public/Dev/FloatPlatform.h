// Artem

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "FloatPlatform.generated.h"

UCLASS()
class YEP_API AFloatPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	AFloatPlatform();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Mesh;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fly")
	TArray<ATargetPoint*> TargetPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fly")
	float FlySpeed = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fly")
	float IdleTime = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fly")
	float DamageAttack = 0.0f;

private:
	int32 IndexTargetPoint;

	void Fly(float DeltaTime);

	FVector FlyRotation;

	FVector CurrentPoint;

	FVector NextPoint;

	ATargetPoint* StartLocation;

	FTimerHandle PatrolTimer;

	bool bOnPoint;

	void ResetPatrol();
};
