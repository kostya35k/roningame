// moe

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "YepPlayerHUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class YEP_API UYepPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "UI")
	float GetHealthPercent() const;
};
