// moe

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "YepGameHUD.generated.h"

UCLASS()
class YEP_API AYepGameHUD : public AHUD
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	virtual void BeginPlay() override;

};
