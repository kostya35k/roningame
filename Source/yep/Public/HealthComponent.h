// moe

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "HealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDeath)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChanged, float)

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class YEP_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();

	float GetHealth() const
	{
		return Health;
	}

	void SetHealth(float Value)
	{
		Health = Value;
	}

	UFUNCTION(BlueprintCallable, Category = "Health")
	bool IsDead()
	{
		return Health <= 0.0;
	}

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetHealthPercent() const
	{
		return Health / MaxHealth;
	}

	FOnDeath OnDeath;
	FOnHealthChanged OnHealthChanged;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"));
	float MaxHealth = 10000.0f;

	virtual void BeginPlay() override;

private:
	float Health = 100.0f;

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy,
		AActor* DamageCauser);
};
