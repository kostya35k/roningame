// Artem

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "YepAICharacter.h"
#include "YepAIHealthBar.generated.h"

UCLASS( Abstract )
class YEP_API UYepAIHealthBar : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetOwner(AYepAICharacter* AICharacter)
	{
		OwnerAICharacter = AICharacter;
	}

protected :
	TWeakObjectPtr<AYepAICharacter> OwnerAICharacter;

	void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;
	
	/** Bar with a percentage of Health */
	UPROPERTY(meta = (BindWidget))
	class UProgressBar* HealthBar;

	/** Number with current amount of health */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* CurrentHealthLabel;

	/** Number with max amount of health */
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* MaxHealthLabel;
};
