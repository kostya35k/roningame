// Artem

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "TimerManager.h"
#include "YepAICharacter.generated.h"

class UYepWeaponComponent;

UCLASS()
class YEP_API AYepAICharacter : public ACharacter
{
	GENERATED_BODY()
public:
	AYepAICharacter();

	float GetHealth()
	{
		return Health;
	}

	void SetHealth(float value)
	{
		Health = value;
	}
	
	float GetMaxHealth()
	{
		return MaxHealth;
	}

	void SetMaxHealth(float value)
	{
		MaxHealth = value;
	}

	void ChangeTakeDamageCoef(float DeltaTime, float value);

	void ChangeMakeDamageCoef(float DeltaTime, float value);

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UYepWeaponComponent* WeaponComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	TArray<ATargetPoint*> TargetPoints;

	UCharacterMovementComponent* CharacterMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Sight")
	float SightRadius = 500.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Sight")
	float SightOutRadius = 1500.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Battle")
	float AttackDistance = 150.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Battle")
	float AttackDamage = 15.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Battle")
	float AttackCooldown = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Battle")
	float MakeDamageCoef = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Battle")
	float TakeDamageCoef = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle")
	float SpeedHealthRecovery;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UWidgetComponent* HealthWidgetComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Battle")
	float Health;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Battle")
	float MaxHealth = 100.0f;

private:

	FTimerHandle TakeDamageTimer;

	FTimerHandle MakeDamageTimer;

	void OnDeath();

	void ResetTakeDamageTimer();

	void ResetMakeDamageTimer();

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
