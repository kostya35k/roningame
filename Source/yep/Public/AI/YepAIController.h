// Artem

#pragma once

#include "AIController.h"
#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "TimerManager.h"
#include "Perception/PawnSensingComponent.h"
#include "YepAICharacter.h"

#include "YepAIController.generated.h"

enum class Tasks
{
	Escape,
	Attack,
	None,
	Patrol
};

UCLASS()
class YEP_API AYepAIController : public AAIController
{
	GENERATED_BODY()
public:
	AYepAIController();
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual FRotator GetControlRotation() const override;

	UFUNCTION()
	void OnPawnDetected(AActor* DetectedPawn);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Waypoints")
	int32 IndexTargetPoints = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Waypoints")
	float IdleTime = 1.0f;

private:
	Tasks Task;

	void GetCurrentTask();

	FVector MyLocation;

	void Attack(APawn* Player);

	void FireLineTrace(FHitResult& HitResult);

	FTimerHandle PatrolTimer;

	bool bOnPoint;

	void ResetPatrol();

	FTimerHandle AttackTimer;

	bool bCanAttack;

	void ResetAttack();

	void ByPassTargetPoints();

	void Escape(AActor* Other);

	bool bIsAfraidPlayer = false;

	void UpdateAfraid();

	bool bIsPlayerDetected = false;

	float DistanceToTargetPoint;

	float DistanceToPlayer;

	AYepAICharacter* MyCharacter = nullptr;
};
