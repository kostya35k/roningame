// moe

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "YepBaseWeapon.generated.h"


class USketetalMeshComponent;

UCLASS()
class YEP_API AYepBaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	AYepBaseWeapon();

	virtual void Attack();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FName BladeSocketName = "BladeSocket";

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float HitMaxDistance = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DamageAmount = 25.0f;

	virtual void BeginPlay() override;

	void MakeHit();
	void MakeDamage(const FHitResult& HitResult);
	APlayerController* GetPlayerController() const;
};
