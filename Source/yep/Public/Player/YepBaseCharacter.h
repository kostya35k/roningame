#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/InventoryComponent.h"
#include "Components/Item.h"
#include "YepBaseCharacter.generated.h"

class UCameraComponent;
class UHealthComponent;
class UTextRenderComponent;
class UYepWeaponComponent;
class USpringArmComponent;
class UInventoryComponent;

UCLASS()
class YEP_API AYepBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AYepBaseCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UInventoryComponent* Inventory;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UTextRenderComponent* HealthTextComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UYepWeaponComponent* WeaponComponent;

	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void ChangeHP(float Value);

	UFUNCTION(BlueprintCallable, Category = "Item")
	void UseItem(class UItem* Item);

private:
	void MoveRight(float Amount);

	void OnDeath();

	void OnHealthChanged(float Health);

	void Attack();
};
