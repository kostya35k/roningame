// moe

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Player/YepBaseCharacter.h"
#include "PlayerAnimation.generated.h"

/**
 * 
 */
UCLASS()
class YEP_API UPlayerAnimation : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, Category = "Anim")
	FString RotationAxis = "Yaw";

	UPROPERTY(EditAnywhere, Category = "Anim")
	UStaticMeshComponent* RotableMesh;

	UPROPERTY(EditAnywhere, Category = "Anim")
	float MaxRotation;

	void ChangeRot();
};
