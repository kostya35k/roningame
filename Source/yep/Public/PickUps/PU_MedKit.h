// moe

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PU_MedKit.generated.h"

UCLASS()
class YEP_API APU_MedKit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APU_MedKit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
