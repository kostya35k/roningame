// moe

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/Item.h"
#include "Components/StaticMeshComponent.h"
#include "YepBasePickUp.generated.h"

class USphereComponent;

UCLASS()
class YEP_API AYepBasePickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	AYepBasePickUp();

protected:
	UPROPERTY(VisibleAnywhere, Category = "PickUps")
	USphereComponent* CollisionComponent;

	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:
	virtual void Tick(float DeltaTime) override;

};
