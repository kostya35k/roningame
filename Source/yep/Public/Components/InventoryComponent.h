// moe

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnIventoryUpdated);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class YEP_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInventoryComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Item")
	bool AddItem(class UItem* Item);

	UFUNCTION(BlueprintCallable, Category = "Item")
	bool RemoveItem(class UItem* Item);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
	TArray<class UItem*> PlayerItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	int32 Capacity;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnIventoryUpdated OnIventoryUpdated;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Instanced)
	TArray<class UItem*> DefaultItems;
};
