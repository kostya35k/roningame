// moe

#pragma once

#include "Components/StaticMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "CoreMinimal.h"
#include "Player/YepBaseCharacter.h"
#include "UObject/NoExportTypes.h"
#include "Internationalization/Text.h"
#include "Item.generated.h"

/**
 *
 */
UCLASS(Abstract, BlueprintType, Blueprintable, EditInLineNew, DefaultToInstanced)
// UCLASS()
class YEP_API UItem : public UObject
{
	GENERATED_BODY()

public:
	UItem();

	virtual class UWorld* GetWorld() const
	{
		return World;
	}

	UPROPERTY(Transient)
	class UWorld* World;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	class UStaticMesh* ItemMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	class UTexture2D* Thumbnail;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UWidgetComponent* ItemWidget;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UInventoryComponent* OwningInventory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	FText ItemDisplayName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item", meta=(Multyline=true))
	FText ItemDescription;

	virtual void Use(class AYepBaseCharacter* Player);

	// UFUNCTION(BlueprintImplementableEvent)
	// void OnUSe();
};
