// moe

#pragma once

#include "Components/Item.h"
#include "CoreMinimal.h"
#include "Medkit.generated.h"

/**
 *
 */
UCLASS()
class YEP_API UMedkit : public UItem
{
	GENERATED_BODY()

protected:
	void Use(class AYepBaseCharacter* Player) override;
};
