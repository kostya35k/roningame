// moe

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "YepWeaponComponent.generated.h"

class AYepBaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class YEP_API UYepWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UYepWeaponComponent();

	void Attack();

	void OnDeath();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<AYepBaseWeapon> WeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponAttachPointName = "WeaponSocket";

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	AYepBaseWeapon* CurrentWeapon = nullptr;

	void SpawnWeapon();
};
