#include "Player/YepBaseCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/YepWeaponComponent.h"
#include "Components/CapsuleComponent.h"
#include "HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogYepBaseCharacter, All, All)

AYepBaseCharacter::AYepBaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->bDoCollisionTest = false;
	SpringArmComponent->SetUsingAbsoluteRotation(true);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");

	HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>("HealthTextComponent");
	HealthTextComponent->SetupAttachment(GetRootComponent());

	WeaponComponent = CreateDefaultSubobject<UYepWeaponComponent>("WeaponComponent");

	Inventory = CreateDefaultSubobject<UInventoryComponent>("Inventory");
	Inventory->Capacity = 10;

	GetCharacterMovement()->bOrientRotationToMovement = true;
}

void AYepBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(GetCharacterMovement());
	check(HealthComponent);
	check(HealthTextComponent);

	OnHealthChanged(HealthComponent->GetHealth());
	HealthComponent->OnDeath.AddUObject(this, &AYepBaseCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &AYepBaseCharacter::OnHealthChanged);
}

void AYepBaseCharacter::OnHealthChanged(float Health)
{
	HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
}

void AYepBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AYepBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(WeaponComponent);

	PlayerInputComponent->BindAxis("MoveRight", this, &AYepBaseCharacter::MoveRight);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AYepBaseCharacter::Jump);
	/*PlayerInputComponent->BindAction("Attack", IE_Pressed, WeaponComponent, &UYepWeaponComponent::Attack);*/
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AYepBaseCharacter::Attack);
}

void AYepBaseCharacter::Attack()
{
	FHitResult HitResult;
	if (!GetWorld())
		return;

	const FVector LineStart = GetActorLocation();

	const FVector LineEnd = LineStart + GetActorForwardVector() * 150.0f;

	ECollisionChannel CollisionChannel = ECC_Visibility;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(HitResult, LineStart, LineEnd, CollisionChannel, Params))
	{
		DrawDebugLine(GetWorld(), LineStart, HitResult.Location, FColor::Blue, false, .2f, 0, 3.0f);
	}
	else
	{
		DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor::Blue, false, .2f, 0, 3.0f);
	}
	if (HitResult.GetActor())
	{
		UGameplayStatics::ApplyDamage(
			HitResult.GetActor(), 10, nullptr, this, nullptr);
	}
}

void AYepBaseCharacter::MoveRight(float Amount)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), Amount);
	//AddMovementInput(GetActorForwardVector(), Amount);
}

void AYepBaseCharacter::OnDeath()
{

	GetCharacterMovement()->DisableMovement();

	//SetLifeSpan(2.0f);

	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	Destroy();
}

void AYepBaseCharacter::ChangeHP(float Value)
{
	float HP = HealthComponent->GetHealth();
	if (HP + Value >= 100)
	{
		HealthComponent->SetHealth(100);
	}
	else
	{
		HealthComponent->SetHealth(HP + Value);
	}
}

void AYepBaseCharacter::UseItem(class UItem* Item)
{
	if (Item)
	{
		Item->Use(this);
	}
}

