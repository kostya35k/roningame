// moe


#include "PickUps/BU_Item.h"

// Sets default values
ABU_Item::ABU_Item()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
	SetRootComponent(CollisionComponent);

	ItemMesh = CreateDefaultSubobject<UStaticMesh>("ItemMesh");
	Item = CreateDefaultSubobject<UItem>("Item");
}

// Called when the game starts or when spawned
void ABU_Item::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABU_Item::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABU_Item::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	Destroy();
}

