// moe


#include "PickUps/YepBasePickUp.h"
#include "Components/SphereComponent.h"
#include "Components/Item.h"
#include "Player/YepBaseCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogBasePickUp, All, All);

AYepBasePickUp::AYepBasePickUp()
{
 	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);
}

void AYepBasePickUp::BeginPlay()
{
	Super::BeginPlay();
}

void AYepBasePickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AYepBasePickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	UE_LOG(LogBasePickUp, Display, TEXT("Picked Up!!"));
	Destroy();
}


