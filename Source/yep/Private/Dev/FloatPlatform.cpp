// Artem

#include "Dev/FloatPlatform.h"
#include "Math/Vector.h"
#include "Math/Rotator.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

AFloatPlatform::AFloatPlatform()
{
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SetRootComponent(Mesh);
}

void AFloatPlatform::BeginPlay()
{
	Super::BeginPlay();
	IndexTargetPoint = 0;
	CurrentPoint = GetActorLocation();
	FRotator Rotation(0, 0, 0);
	FActorSpawnParameters SpawnParams;
	StartLocation = GetWorld()->SpawnActor<ATargetPoint>(ATargetPoint::StaticClass(), CurrentPoint, Rotation, SpawnParams);
	TargetPoints.Add(StartLocation);
	if (TargetPoints.Num() > 1)
	{
		NextPoint = TargetPoints[IndexTargetPoint]->GetActorLocation();
		FlyRotation = (NextPoint - CurrentPoint) / FVector::Dist(CurrentPoint, NextPoint);
	}
}

void AFloatPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurrentPoint = GetActorLocation();
	Fly(DeltaTime);
}

void AFloatPlatform::Fly(float DeltaTime)
{
	/*if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(
			-1, 2.f, FColor::Green, FString::Printf(TEXT("Platform Location is %s"), *CurrentPoint.ToString()));
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("TargetPoint is %d"), IndexTargetPoint));
		GEngine->AddOnScreenDebugMessage(
			-1, 2.f, FColor::Green, FString::Printf(TEXT("Target Location is %s"), *NextPoint.ToString()));
	}*/
	if (!bOnPoint && TargetPoints.Num() > 0)
	{
		if (FVector::Dist(CurrentPoint, NextPoint) <= 50.0f)
		{
			IndexTargetPoint++;
			if (IndexTargetPoint == TargetPoints.Num())
			{
				IndexTargetPoint = 0;
			}
			NextPoint = TargetPoints[IndexTargetPoint]->GetActorLocation();
			FlyRotation = (NextPoint - CurrentPoint) / FVector::Dist(CurrentPoint, NextPoint);
			bOnPoint = true;
			GetWorld()->GetTimerManager().SetTimer(PatrolTimer, this, &AFloatPlatform::ResetPatrol, IdleTime, false);
		}

		FHitResult HitResult;
		FVector NewLocation = CurrentPoint + FlyRotation * DeltaTime * FlySpeed;
		SetActorLocation(NewLocation, true, &HitResult);
		if (HitResult.GetActor())
		{
			/*if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(
					-1, 2.f, FColor::Red, FString::Printf(TEXT("Platform Location is %s"), *CurrentPoint.ToString()));
				GEngine->AddOnScreenDebugMessage(
					-1, 2.f, FColor::Red, HitResult.GetActor()->GetFName().ToString());
			}*/
			UGameplayStatics::ApplyDamage(HitResult.GetActor(), DamageAttack, nullptr, this, nullptr);
		}
	}
}

void AFloatPlatform::ResetPatrol()
{
	bOnPoint = false;
	GetWorldTimerManager().ClearTimer(PatrolTimer);
}

