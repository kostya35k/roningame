// Artem


#include "Dev/DamagerBox.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"


ADamagerBox::ADamagerBox()
{
	PrimaryActorTick.bCanEverTick = true;
	OnActorBeginOverlap.AddDynamic(this, &ADamagerBox::BeginOnOverlap);
	OnActorEndOverlap.AddDynamic(this, &ADamagerBox::EndOnOverlap);
}

void ADamagerBox::BeginPlay()
{
	Super::BeginPlay();

	CurrentTime = ActiveTime;
	DrawDebugBox(GetWorld(), GetActorLocation(), GetComponentsBoundingBox().GetExtent(), Color, true, -1, 0, 10);
}

void ADamagerBox::Tick(float DeltaTime)
{
	CurrentTime -= ActiveTime;
	if (CurrentTime <= 0.0f)
	{
		if (bIsActive)
		{
			bIsActive = false;
			CurrentTime = ActiveCooldown;
		}
		else
		{
			bIsActive = true;
			CurrentTime = ActiveTime;
		}
	}
}

void ADamagerBox::ResetTimer()
{
	GetWorldTimerManager().ClearTimer(Timer);
	if (TargetActor != nullptr)
	{
		if (bIsActive)
		{
			UGameplayStatics::ApplyDamage(TargetActor, DamageAttack, nullptr, this, nullptr);
		}
		GetWorld()->GetTimerManager().SetTimer(Timer, this, &ADamagerBox::ResetTimer, DamageCooldown, false);
	}
}

void ADamagerBox::BeginOnOverlap(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (!OtherActor)
	{
		return;
	}
	if (OtherActor != this)
	{
		TargetActor = OtherActor;
		/*if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, OverlappedActor->GetFName().ToString());
		}
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, TargetActor->GetFName().ToString());
		}*/
		if (bIsActive)
		{
			UGameplayStatics::ApplyDamage(TargetActor, DamageAttack, nullptr, this, nullptr);
		}
		GetWorld()->GetTimerManager().SetTimer(Timer, this, &ADamagerBox::ResetTimer, DamageCooldown, false);
	}
}

void ADamagerBox::EndOnOverlap(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (!OtherActor)
	{
		return;
	}
	if (OtherActor != this)
	{
		/*if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, OverlappedActor->GetFName().ToString());
		}
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Blue, OtherActor->GetFName().ToString());
		}*/
		TargetActor = nullptr;
	}
}