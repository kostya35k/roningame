// Artem

#include "AI/YepAIHealthBar.h"
#include <Components/ProgressBar.h>
#include <Components/TextBlock.h>

void UYepAIHealthBar::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry, DeltaTime);

	if (!OwnerAICharacter.IsValid())
	{
		return;
	}
	float Health = OwnerAICharacter->GetHealth();
	float MaxHealth = OwnerAICharacter->GetMaxHealth();
	HealthBar->SetPercent(Health / MaxHealth);
	FNumberFormattingOptions Opts;
	Opts.SetMaximumFractionalDigits(0);
	CurrentHealthLabel->SetText(FText::AsNumber(Health, &Opts));
	MaxHealthLabel->SetText(FText::AsNumber(MaxHealth, &Opts));
}