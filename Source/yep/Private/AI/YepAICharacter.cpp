// Artem

#include "AI/YepAICharacter.h"
#include "AI/YepAIHealthBar.h"
#include "Components/YepWeaponComponent.h"


AYepAICharacter::AYepAICharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	Health = MaxHealth / 4;
	SpeedHealthRecovery = 0.05f;

	HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBar"));
	HealthWidgetComponent->SetupAttachment(GetRootComponent());
	HealthWidgetComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	CharacterMovement = GetCharacterMovement();
	CharacterMovement->MaxWalkSpeed = 200.0f;

	WeaponComponent = CreateDefaultSubobject<UYepWeaponComponent>("WeaponComponent");
}

void AYepAICharacter::BeginPlay()
{
	Super::BeginPlay();

	UYepAIHealthBar* AIHealthBar = Cast<UYepAIHealthBar>(HealthWidgetComponent->GetUserWidgetObject());
	AIHealthBar->SetOwner(this);
}

void AYepAICharacter::ChangeTakeDamageCoef(float DeltaTime, float value)
{
	TakeDamageCoef = value;
	GetWorld()->GetTimerManager().SetTimer(TakeDamageTimer, this, &AYepAICharacter::ResetTakeDamageTimer, DeltaTime, false);
}

void AYepAICharacter::ChangeMakeDamageCoef(float DeltaTime, float value)
{
	MakeDamageCoef = value;
	GetWorld()->GetTimerManager().SetTimer(MakeDamageTimer, this, &AYepAICharacter::ResetMakeDamageTimer, DeltaTime, false);
}

void AYepAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Health <= 0)
	{
		OnDeath();
	}
	if (Health > 0 && Health < MaxHealth)
	{
		Health += SpeedHealthRecovery;
	}
}

void AYepAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


void AYepAICharacter::OnDeath()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, FString::Printf(TEXT("AI is dead")));	
	}
	WeaponComponent->OnDeath();
	Destroy();
}

void AYepAICharacter::ResetTakeDamageTimer()
{
	GetWorldTimerManager().ClearTimer(TakeDamageTimer);
	TakeDamageCoef = 1.0f;
}

void AYepAICharacter::ResetMakeDamageTimer()
{
	GetWorldTimerManager().ClearTimer(MakeDamageTimer);
	MakeDamageCoef = 1.0f;
}

float AYepAICharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Health -= DamageAmount * TakeDamageCoef;

	return DamageAmount * TakeDamageCoef;
}
