// Artem

#include "AI/YepAIController.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Vector.h"
#include "Math/UnrealMathUtility.h"
#include "Navigation/PathFollowingComponent.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"

AYepAIController::AYepAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AYepAIController::BeginPlay()
{
	Super::BeginPlay();

	bCanAttack = true;

	bIsAfraidPlayer = false;

	bOnPoint = false;

	MyCharacter = Cast<AYepAICharacter>(GetPawn());

	Task = Tasks::None;
}

void AYepAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	bCanAttack = true;

	bIsAfraidPlayer = false;

	bOnPoint = false;

	MyCharacter = Cast<AYepAICharacter>(InPawn);

	Task = Tasks::None;
}

void AYepAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	APawn* Player = GetWorld()->GetFirstPlayerController()->GetPawn();
	MyLocation = MyCharacter->GetActorLocation();
	if (Player == nullptr)
	{
		ByPassTargetPoints();
		return;
	}
	FVector PlayerLocation = Player->GetActorLocation();
	DistanceToPlayer = FVector::Dist(MyLocation, PlayerLocation);
	if (DistanceToPlayer > MyCharacter->SightOutRadius)
	{
		bIsPlayerDetected = false;
	}
	if (DistanceToPlayer < MyCharacter->SightRadius)
	{
		bIsPlayerDetected = true;
	}
	UpdateAfraid();
	if (Task == Tasks::Escape && GetMoveStatus() == EPathFollowingStatus::Idle)
	{
		Task = Tasks::None;
	}
	if (Task == Tasks::Attack && (!bIsPlayerDetected || !bCanAttack))
	{
		Task = Tasks::None;
	}
	if (Task == Tasks::Patrol || Task == Tasks::None)
	{
		GetCurrentTask();
	}
	switch (Task)
	{
		case Tasks::Attack:
			MyCharacter->CharacterMovement->MaxWalkSpeed = 500.0f;
			/*GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Attack")));*/
			Attack(Player);
			break;

		case Tasks::Patrol:
			MyCharacter->CharacterMovement->MaxWalkSpeed = 200.0f;
			/*GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Patrol")));*/
			ByPassTargetPoints();
			break;

		case Tasks::Escape:
			MyCharacter->CharacterMovement->MaxWalkSpeed = 500.0f;
			/*GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, FString::Printf(TEXT("Escape")));*/
			Escape(Player);
			break;
	}
}

FRotator AYepAIController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}
	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);
}

void AYepAIController::OnPawnDetected(AActor* DetectedPawn)
{
	DistanceToPlayer = GetPawn()->GetDistanceTo(DetectedPawn);
	bIsPlayerDetected = true;
}

void AYepAIController::GetCurrentTask()
{
	if (bIsPlayerDetected)
	{
		if (!bIsAfraidPlayer)
		{
			Task = Tasks::Attack;
		}
		else
		{
			Task = Tasks::Escape;
		}
	}
	else
	{
		Task = Tasks::Patrol;
	}
}

void AYepAIController::Attack(APawn* Player)
{
	if (DistanceToPlayer < MyCharacter->AttackDistance && bCanAttack)
	{
		bCanAttack = false;
		FHitResult HitResult;
		FireLineTrace(HitResult);
		if (HitResult.GetActor())
		{
			UGameplayStatics::ApplyDamage(
				HitResult.GetActor(), MyCharacter->AttackDamage * MyCharacter->MakeDamageCoef, this, MyCharacter, nullptr);
		}
		GetWorld()->GetTimerManager().SetTimer(
			AttackTimer, this, &AYepAIController::ResetAttack, MyCharacter->AttackCooldown, false);
	}
	else
	{
		MoveToActor(Player, 50.0f);
	}
}

void AYepAIController::FireLineTrace(FHitResult& HitResult)
{
	if (!GetWorld())
		return;

	const FVector LineStart = MyCharacter->GetActorLocation();

	const FVector LineEnd = LineStart + MyCharacter->GetActorForwardVector() * MyCharacter->AttackDistance;

	ECollisionChannel CollisionChannel = ECC_Visibility;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(MyCharacter);

	if (GetWorld()->LineTraceSingleByChannel(HitResult, LineStart, LineEnd, CollisionChannel, Params))
	{
		DrawDebugLine(GetWorld(), LineStart, HitResult.Location, FColor::Blue, false, .2f, 0, 3.0f);
	}
	else
	{
		DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor::Blue, false, .2f, 0, 3.0f);
	}
}

void AYepAIController::ResetAttack()
{
	bCanAttack = true;
	GetWorldTimerManager().ClearTimer(AttackTimer);
}

void AYepAIController::ByPassTargetPoints()
{
	if (!bOnPoint)
	{
		MoveToActor(MyCharacter->TargetPoints[IndexTargetPoints], 1.0f);
		DistanceToTargetPoint =
			FVector::Dist(MyLocation, Cast<AActor>(MyCharacter->TargetPoints[IndexTargetPoints])->GetActorLocation());
		if (DistanceToTargetPoint <= 50.0f)
		{
			IndexTargetPoints = FMath::RandRange(0, MyCharacter->TargetPoints.Num() - 1);
			bOnPoint = true;
			GetWorld()->GetTimerManager().SetTimer(PatrolTimer, this, &AYepAIController::ResetPatrol, IdleTime, false);
		}
	}
}

void AYepAIController::ResetPatrol()
{
	bOnPoint = false;
	GetWorldTimerManager().ClearTimer(PatrolTimer);
}

void AYepAIController::UpdateAfraid()
{
	if (!MyCharacter)
	{
		return;
	}
	if (MyCharacter->GetHealth() * 3 < MyCharacter->GetMaxHealth())
	{
		bIsAfraidPlayer = true;
	}
	else
	{
		bIsAfraidPlayer = false;
	}
}

void AYepAIController::Escape(AActor* Other)
{
	if (!Other)
	{
		return;
	}
	AActor* BestActor = nullptr;
	float BestDistance = 0.0f;
	for (int32 i = 0; i < MyCharacter->TargetPoints.Num(); ++i)
	{
		FVector ActorLocation = Cast<AActor>(MyCharacter->TargetPoints[i])->GetActorLocation();
		float MyDistance = FVector::Dist(MyLocation, ActorLocation);
		float OtherDistance = FVector::Dist(Other->GetActorLocation(), ActorLocation);
		/*if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green,
				FString::Printf(TEXT("MyDistance %d for Escape is %f"), i, MyDistance));
			GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green,
				FString::Printf(TEXT("OtherDistance %d for Escape is %f"), i, OtherDistance));
		}*/
		if (MyDistance < OtherDistance && BestDistance < MyDistance)
		{
			BestDistance = MyDistance;
			BestActor = MyCharacter->TargetPoints[i];
		}
	}
	if (!BestActor)
	{
		Task = Tasks::Attack;
		return;
	}
	/*if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green,
			FString::Printf(TEXT("Best Location for Escape is %s"), *(BestActor->GetActorLocation()).ToString()));
	}*/
	MoveToActor(BestActor, 5.0f);
}
