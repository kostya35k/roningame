// moe

#include "Weapon/YepBaseWeapon.h"

#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "DrawdebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogYepBaseWeapon, All, All)

AYepBaseWeapon::AYepBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
	SetRootComponent(WeaponMesh);
}

void AYepBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(WeaponMesh);
}

void AYepBaseWeapon::Attack()
{
	UE_LOG(LogYepBaseWeapon, Display, TEXT("Attack!"));

	MakeHit();
}

void AYepBaseWeapon::MakeHit()
{
	if (!GetWorld())
		return;

	const FTransform SocketTransform = WeaponMesh->GetSocketTransform(BladeSocketName);
	const FVector TraceStart = SocketTransform.GetLocation();
	const FVector HitDirection = SocketTransform.GetRotation().GetForwardVector();
	const FVector TraceEnd = TraceStart + HitDirection * HitMaxDistance;
	DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 3.0f, 0, 3.0f);

	FCollisionQueryParams CollisionParams;

	CollisionParams.AddIgnoredActor(GetPlayerController());

	FHitResult HitResult; 
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);

	if (HitResult.bBlockingHit)
	{
		MakeDamage(HitResult);

		DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10.0f, 24, FColor::Red, false, 5.0f);

		UE_LOG(LogYepBaseWeapon, Display, TEXT("Bone: %s"), *HitResult.BoneName.ToString());
	}
}

void AYepBaseWeapon::MakeDamage(const FHitResult& HitResult)
{
	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor)
		return;

	DamagedActor->TakeDamage(DamageAmount, FDamageEvent(), GetPlayerController(), this);
}

APlayerController* AYepBaseWeapon::GetPlayerController() const
{
	const auto Player = Cast<ACharacter>(GetOwner());
	if (!Player)
		return nullptr;

	return Player->GetController<APlayerController>();
}
