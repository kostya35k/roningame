// moe


#include "YepGameModeBase.h"
#include "Player/YepBaseCharacter.h"
#include "Player/YepPlayerController.h"

AYepGameModeBase::AYepGameModeBase() {
    DefaultPawnClass = AYepBaseCharacter::StaticClass();
    PlayerControllerClass = AYepPlayerController::StaticClass();
}

