// moe


#include "UI/YepGameHUD.h"
#include "Blueprint/UserWidget.h"

void AYepGameHUD::BeginPlay()
{
	Super::BeginPlay();
	auto PlayerHUDWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidgetClass);

	if (PlayerHUDWidget)
	{
		PlayerHUDWidget->AddToViewport();
	}
}
