// moe

#include "Components/InventoryComponent.h"

#include "Components/Item.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	Capacity = 10;
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	for (auto& Item : DefaultItems)
	{
		AddItem(Item);
	}
}

bool UInventoryComponent::AddItem(class UItem* Item)
{
	if (PlayerItems.Num() >= Capacity || !Item)
	{
		return false;
	}
	Item->OwningInventory = this;
	Item->World = GetWorld();
	PlayerItems.Add(Item);
	OnIventoryUpdated.Broadcast();
	return true;
}

bool UInventoryComponent::RemoveItem(class UItem* Item)
{
	if (Item)
	{
		Item->OwningInventory = nullptr;
		Item->World = nullptr;
		PlayerItems.RemoveSingle(Item);
		OnIventoryUpdated.Broadcast();
		return true;
	}
	return false;
}
