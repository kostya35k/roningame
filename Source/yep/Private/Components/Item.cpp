// moe

#include "Components/Item.h"

UItem::UItem()
{
	ItemMesh = CreateDefaultSubobject<UStaticMesh>("ItemMesh");
	Thumbnail = CreateDefaultSubobject<UTexture2D>("Thumbnail");
	ItemWidget = CreateDefaultSubobject<UWidgetComponent>("ItemWidget");
	ItemDisplayName = FText::FromString("ItemName");
	ItemDescription = FText::FromString("ItemDescription");
	OwningInventory = nullptr;
}

void UItem::Use(class AYepBaseCharacter* Player)
{
}
