// moe


#include "Components/YepWeaponComponent.h"
#include "Weapon/YepBaseWeapon.h"
#include "GameFramework/Character.h"

UYepWeaponComponent::UYepWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UYepWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	SpawnWeapon();
}

void UYepWeaponComponent::SpawnWeapon()
{
	if (!GetWorld())
		return;
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character)
		return;
	CurrentWeapon = GetWorld()->SpawnActor<AYepBaseWeapon>(WeaponClass);
	if (!CurrentWeapon)
		return;
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	CurrentWeapon->AttachToComponent(Character->GetMesh(), AttachmentRules, WeaponAttachPointName);
}

void UYepWeaponComponent::Attack() 
{
	if (!CurrentWeapon)
		return;
	CurrentWeapon->Attack();
}

void UYepWeaponComponent::OnDeath()
{
	CurrentWeapon->Destroy();
	CurrentWeapon = nullptr;
}