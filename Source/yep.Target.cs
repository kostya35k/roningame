// moe

using UnrealBuildTool;
using System.Collections.Generic;

public class yepTarget : TargetRules
{
	public yepTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "yep" } );
	}
}
